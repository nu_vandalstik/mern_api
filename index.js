const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer");
const path = require('path')
const authRoutes = require("./src/routes/auth");
const blogRoutes = require("./src/routes/blog");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().getTime() + "-" + file.originalname)
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(bodyParser.json()); //type json yg akan di terima

app.use('/images', express.static(path.join(__dirname, 'images')))
app.use(
  multer({
    storage: fileStorage,
    fileFilter: fileFilter,
  }).single("image")
);

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"); //IZIN MENGAKSES API
  // 'https://codepen.io')
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  ); // IZIN MENGAKSES METHODS
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization"); //headers apa saja yg boleh dikirim
  next();
});

app.use("/v1/auth", authRoutes);
app.use("/v1/blog", blogRoutes);
// app.use(productRoutes);

app.use((error, req, res, next) => {
  const status = error.errorStatus || 500;
  const message = error.message;
  const data = error.data;

  res.status(status).json({ message: message, data: data });
});

mongoose
  .connect(
    
       "mongodb+srv://wisnu:7F1oKhRFTtDqY0Km@cluster0.9sr2w.mongodb.net/blog?retryWrites=true&w=majority" ,
        {useNewUrlParser: true, useUnifiedTopology: true}
    // "mongodb+srv://wisnu:7F1oKhRFTtDqY0Km@cluster0.9sr2w.mongodb.net/blog?retryWrites=true&w=majority"
  )
  .then(() => {
    app.listen(4000, console.log("koneksi berhasil"));
  })
  .catch((err) => console.log(err));

  // 180.251.176.97